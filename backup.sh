#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/config.cfg"
echo "$(date) ...starting backup" | tee  backup.log


echo "$(date) ...set the bak rotate folder" | tee -a backup.log
if [ $rotate == "weekly" ]
then
  bak="${bak_dir}/wd$(date +"%u")"
else
  bak="${bak_dir}/wd$(date +"%d")"
fi


#delete and create backupdir
echo "$(date) ...remove backup with the same day: $bak" | tee -a backup.log
rm $bak -Rf
mkdir -p "${bak}"


#backup postgres db
if [ -z ${pg_host} ] || [ -z ${pg_user} ] || [ -z ${pg_db} ]  || [ -z ${pg_pass} ]  
then
  echo "$(date) ...no postgres db for backup" | tee -a backup.log
else
  echo "$(date) ...backup postgres db: ${pg_db}" | tee -a backup.log
  echo "*:*:${pg_db}:${pg_user}:${pg_pass}" > "${HOME}/.pgpass"
  chmod 600 "${HOME}/.pgpass"
  pg_dump -w -h$pg_host -U $pg_user $pg_db | gzip -c > "${bak}/postgres-redmine.sql.gz"  
  #pg_dumpall -h$pg_host -U $pg_user  | gzip -c > "${bak}/postgres-all.sql.gz"
fi

#backup mariadb/mysql
if [ -z ${mysql_host} ] || [ -z ${mysql_user} ]  || [ -z ${mysql_pass} ] 
then
  echo "$(date) ...no mysql/mariadb for backup" | tee -a backup.log
else
  databases=`mysql --host=$mysql_host --user=$mysql_user -p$mysql_pass -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema|performance_schema)"`
  for db in $databases
    do
      echo "$(date) ...backup mysql db: ${db}" | tee -a backup.log  
      mysqldump --force --opt --user=$mysql_user -p$mysql_pass --databases $db | gzip > "${bak}/mysql-${db}.gz"
    done
fi

#backup folders
for i in ${dirs[@]}
do
  folder="${i//\//_}"
  echo "$(date) ...backup ${folder}" | tee -a backup.log
  tar -cvpzf "${bak}/${folder}.tar.gz" $i >> backup.log
done


#report script to be sure the backup was done
if [ -z ${report_script} ]
then
  echo "$(date) ...no report script" | tee -a backup.log  
else  
  echo "$(date) ...exec report script to be sure the backup was done" | tee -a backup.log  
  $report_script
fi


echo "$(date) ...end backup" | tee -a backup.log


